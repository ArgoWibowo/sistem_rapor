<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->helper('pdf_helper');
        $this->load->library('grocery_CRUD');
		$this->load->library('session');
        $this->load->model('User_model');
        $this->load->model('Nilai_model');
        $this->load->model('Mata_pelajaran_model');
 	}

 	function Index(){
        if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['menu'] = 'home';
                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('index', $data);
                $this->load->view('footer_admin', $data);
        }else{
        	if(empty($data)){
 				$data = new stdClass();
 			}
 			$data->menu = 'login';
			$this->load->helper ( array (
					'form' 
			) );
			$this->load->view ( 'header2', $data );
			$this->load->view ( 'login', $data );
    	}
 	}

    function siswa(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('siswa');
                $crud->set_subject('Siswa');
                $crud->required_fields('ID_SISWA','NAMA_SISWA');
                $crud->columns('ID_SISWA','NAMA_SISWA');

                $crud->display_as('ID_SISWA','NOMOR INDUK');

                $data = $crud->render();

                $data->menu = 'setup';

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('siswa_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function mata_pelajaran(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('mata_pelajaran');
                $crud->set_subject('Mata Pelajaran');
                $crud->required_fields('KODE_MAPEL','NAMA_MAPEL');
                $crud->columns('KODE_MAPEL','NAMA_MAPEL');

                $crud->display_as('KODE_MAPEL','KODE MATA PELAJARAN')
                    ->display_as('NAMA_MAPEL','NAMA MATA PELAJARAN');

                $data = $crud->render();

                $data->menu = 'setup';

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('mata_pelajaran_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function tahun_ajaran(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('tahun_ajaran');
                $crud->set_subject('Tahun Ajaran');
                $crud->required_fields('TAHUN');
                $crud->columns('ID_TAHUN_AJARAN','TAHUN');

                $crud->display_as('TAHUN','TAHUN AJARAN');

                $data = $crud->render();

                $data->menu = 'setup';

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('tahun_ajaran_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function guru(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('guru');
                $crud->set_subject('Guru');
                $crud->required_fields('NIK','NAMA_GURU');
                $crud->columns('NIK','NAMA_GURU');

                $data = $crud->render();

                $data->menu = 'setup';

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('guru_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function kelas(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('kelas');
                $crud->set_subject('Kelas');
                $crud->required_fields('NIK','NAMA_KELAS');
                $crud->columns('NIK','NAMA_KELAS');
                $crud->set_relation('NIK','guru','NAMA_GURU');

                $crud->display_as('NIK','NAMA GURU WALI');

                $data = $crud->render();

                $data->menu = 'setup';

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('kelas_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function setting_nilai(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('setting_nilai');
                $crud->set_subject('Setting Batas Nilai');
                $crud->required_fields('huruf','angka','predikat','deskripsi');
                $crud->columns('huruf','angka','predikat','deskripsi');

                $data = $crud->render();

                $data->menu = 'setup';

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('kelas_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function mapel_kelas(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('kelas');
                $crud->set_subject('Mata Pelajaran per Kelas');
                $crud->columns('ID_KELAS','KODE_MAPEL');
                
                $crud->set_relation('ID_KELAS','kelas','NAMA_KELAS');
                
                $crud->set_relation_n_n('KODE_MAPEL', 'mata_pelajaran_kelas', 'mata_pelajaran', 'ID_KELAS', 'KODE_MAPEL', 'NAMA_MAPEL');

                $crud->display_as('ID_KELAS','KELAS')
                    ->display_as('KODE_MAPEL','NAMA MATA PELAJARAN');

                $crud->fields('ID_KELAS','KODE_MAPEL');

                $data = $crud->render();

                $data->menu = 'setup';

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('mapel_kelas_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function siswa_kelas(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $crud = new grocery_CRUD();

                $crud->set_theme('datatables');
                $crud->set_table('kelas');
                $crud->set_subject('Siswa per Kelas');
                $crud->columns('ID_KELAS','ID_SISWA');
                
                $crud->set_relation('ID_KELAS','kelas','NAMA_KELAS');
                
                $crud->set_relation_n_n('ID_SISWA', 'siswa_kelas', 'siswa', 'ID_KELAS', 'ID_SISWA', 'NAMA_SISWA');

                $crud->display_as('ID_KELAS','KELAS')
                    ->display_as('ID_SISWA','NAMA SISWA');

                $crud->fields('ID_KELAS','ID_SISWA');

                $data = $crud->render();

                $data->menu = 'setup';

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('siswa_kelas_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function entri_nilai(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $data['menu'] = 'entri_nilai';

                $data['mata_pelajaran'] = $this->Mata_pelajaran_model->getAllMapel();
                $data['kelas'] = $this->Mata_pelajaran_model->getKelas();
                $data['tahun'] = $this->Mata_pelajaran_model->getTahunAjar();

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('entri_nilai_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function isi_nilai(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['menu'] = 'entri_nilai';

                //start upload excel
                $config['upload_path']          = './assets/uploads/';
                $config['allowed_types']        = 'xls';
                $config['max_size']             = 2000000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('fileToUpload'))
                {
                    $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                    $file_data = $this->upload->data();
                }

                $file_path =  './assets/uploads/'.$file_data['file_name'];
                
                //load the excel library
                $this->load->library('excel');
                
                //read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($file_path);
                 
                //get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

                $id_mapel = $this->input->post('cb_kode_mapel');
                $id_kelas = $this->input->post('cb_kode_kelas');
                $id_tahun = $this->input->post('cb_kode_tahun');

                //extract to a PHP readable array format
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                 
                    if($column == 'A'){
                        $column_id = 'ID_SISWA';
                        $arr_data[$row]['ID_MAPEL'] = $id_mapel;
                        $arr_data[$row]['ID_KELAS'] = $id_kelas;
                        $arr_data[$row]['ID_TAHUN'] = $id_tahun;
                    }else if($column == 'B'){
                        $column_id = 'NILAI';
                    }
                    $arr_data[$row][$column_id] = $data_value;
                }
                //send the data in an array format
                $data['nilai'] = $arr_data;
                
                $data['mata_pelajaran'] = $this->Mata_pelajaran_model->getAllMapel();
                $data['kelas'] = $this->Mata_pelajaran_model->getKelas();
                $data['tahun'] = $this->Mata_pelajaran_model->getTahunAjar();

                $this->Nilai_model->insertNilai($arr_data);

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('entri_nilai_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function cetak_rapor(){
        try{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];

                $data['menu'] = 'cetak_rapor';

                $idSiswa = $this->input->post("IdSiswa");

                if (!empty($idSiswa)){
                    $nilai = $this->Nilai_model->getNilaiSiswa($idSiswa);
                    $data['nilai'] = $nilai;
                }

                $data['id_siswa'] = $idSiswa;

                $this->load->view('header2', $data);
                $this->load->view('header_menu2', $data);
                $this->load->view('cetak_rapor_view', $data);
                $this->load->view('footer_admin', $data);
            }else{
                //If no session, redirect to login page
                redirect('login', 'refresh');
            }
        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function cetak_pdf(){
        $this->load->helper('pdf_helper');
        $this->load->view('rapor_tercetak', $data);
    }

    function logout()
    {
       $this->session->unset_userdata('logged_in');
       session_destroy();
       redirect('login','refresh');
    }
}
?>