<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('unit_test');
		//$this->unit->active(FALSE);
		$this->unit->run(1,'is_int', 'test integer');
		$this->unit->run('2','is_string', 'test string');
		$this->unit->run('FALSE','is_bool', 'test boolean');
		
		$jumlah = 1 + 1;
		
		$this->unit->run($jumlah,4, 'test jumlah');
		$data['testing'] = $this->unit->report();
		
		$this->load->view('welcome_message',$data);
	}
}
