<?php

/**
 * Description of category
 *
 * @author admin
 */
class Mata_pelajaran_model extends CI_Model{

	public function __construct() {
		parent::__construct();
	}

    function getAllMapel(){
        $this -> db -> select("KODE_MAPEL, CONCAT(KODE_MAPEL, ' - ', NAMA_MAPEL) as 'MATA_PELAJARAN' ");
        $this -> db -> from('mata_pelajaran');
/*        $this -> db -> join('mata_pelajaran as mp','mp.KODE_MAPEL = mpk.KODE_MAPEL');
        $this -> db -> join('kelas as k','k.ID_KELAS = mpk.ID_KELAS');*/
        $this -> db -> order_by('NAMA_MAPEL','asc');

        $query = $this -> db -> get();

        if($query -> num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }

    function getTahunAjar(){
        $this -> db -> select("ID_TAHUN_AJARAN, TAHUN ");
        $this -> db -> from('tahun_ajaran');
        $this -> db -> order_by('TAHUN','asc');

        $query = $this -> db -> get();

        if($query -> num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }

    function getKelas(){
        $this -> db -> select("ID_KELAS, NAMA_KELAS");
        $this -> db -> from('kelas');
        $this -> db -> order_by('NAMA_KELAS','asc');

        $query = $this -> db -> get();

        if($query -> num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
}

?>
