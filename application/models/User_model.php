<?php
Class User_model extends CI_Model
{
  function getLoginDetail($id, $password){
      $this -> db -> select('*');
      $this -> db -> from('ms_user');
      $this -> db -> where('username', $id);
      $this -> db -> where('password', $password);
      $this -> db -> where('status',1);
      $this -> db -> limit(1);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1){
        return $query->result();
      }
      else{
        return false;
      }
    }

  function createUser($array_data) {
    $this->db->insert('ms_user', $array_data);
  }
}
?>