<?php

/**
 * Description of category
 *
 * @author admin
 */
class Nilai_model extends CI_Model{

	public function __construct() {
		parent::__construct();
	}
    
    function insertNilai($array_data_nilai) {
        $this->db->trans_start();
        
        foreach($array_data_nilai as $nilai){
            $this->db->insert('nilai', $nilai);
        }
        
        $this->db->trans_complete();
    }

    function getNilaiSiswa($id_siswa){
        $this -> db -> distinct();
        $this -> db -> select("MP.NAMA_MAPEL, NL.NILAI");
        $this -> db -> from('nilai AS NL');
        $this -> db -> join('mata_pelajaran as MP','MP.KODE_MAPEL = NL.ID_MAPEL');
        $this -> db -> where('NL.ID_SISWA',$id_siswa);
        $this -> db -> where('NL.ID_TAHUN',1);
        $this -> db -> order_by('MP.NAMA_MAPEL','asc');

        $query = $this -> db -> get();

        if($query -> num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
}

?>
