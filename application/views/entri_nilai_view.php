<script src="<?php echo base_url();?>assets/grocery_crud/js/jquery-1.11.1.min.js"></script>
<link href="<?php echo base_url();?>assets/Global/css/starter.css" rel="stylesheet">
<?php echo form_open_multipart('Admin/isi_nilai');?>
<form name="form" action="" method="" accept-charset="utf-8">
<div class="container">
    <h1 class="page-header">Admin - Entri Nilai</h1>
    <div>
        <p>Pilih Tahun Ajaran</p>
        <table class="table table-bordered table-striped" id="tblCust">
            <tr>
                <td>
                    <select style="width: 100%" name="cb_kode_tahun">
                    <?php foreach ($tahun as $th): ?>
                        <option value="<?php echo $th->ID_TAHUN_AJARAN?>"><?php echo $th->TAHUN?></option>
                    <?php endforeach; ?>
                    </select>
                </td>
            </tr>
    </table>
    </div>
    <div>
        <p>Pilih Kelas</p>
        <table class="table table-bordered table-striped" id="tblCust">
            <tr>
                <td>
                    <select style="width: 100%" name="cb_kode_kelas">
                    <?php foreach ($kelas as $kl): ?>
                        <option value="<?php echo $kl->ID_KELAS?>"><?php echo $kl->NAMA_KELAS?></option>
                    <?php endforeach; ?>
                    </select>
                </td>
            </tr>
    </table>
    </div>
    <div>
        <p>Pilih Mata Pelajaran</p>
        <table class="table table-bordered table-striped" id="tblCust">
            <tr>
                <td>
                    <select style="width: 100%" name="cb_kode_mapel">
                    <?php foreach ($mata_pelajaran as $mapel): ?>
                        <option value="<?php echo $mapel->KODE_MAPEL?>"><?php echo $mapel->MATA_PELAJARAN?></option>
                    <?php endforeach; ?>
                    </select>
                </td>
            </tr>
	</table>
	</div>
    <div>
    	<?php if(!empty($nilai)){
			print_r('Nilai berhasil diinput');
		}?>
        <p>Pilih File Nilai (Berupa File Excel)</p>
        <table class="table table-bordered table-striped" id="tblItem">
             <tbody>
				<tr>
					<td>
						<input type="file" name="fileToUpload" id="fileToUpload">
			    		<input type="submit" value="Upload" name="submit">
					</td>
				</tr>
              </tbody>
	</table>
    </div>
    
</div>
</form>