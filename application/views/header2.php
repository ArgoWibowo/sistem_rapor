<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?php echo base_url(); ?>assets/images/KPU.png">

<title>Sistem Rapor - Admin</title>

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles -->
<link href="<?php echo base_url();?>assets/Global/css/starter.css" rel="stylesheet">
    <!-- load css and js -->
    <?php //$css_files;
    	if (empty($css_files)){}
    	else{
    		foreach($css_files as $file): ?>
        	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach;} ?>

</head>
