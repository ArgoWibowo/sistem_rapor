<script src="<?php echo base_url();?>assets/grocery_crud/js/jquery-1.11.1.min.js"></script>
<link href="<?php echo base_url();?>assets/Global/css/starter.css" rel="stylesheet">
<script language="Javascript">
    function searchId(){
        var f = document.frmRapor;
        f.method = "POST";
        f.action = "<?php echo site_url('Admin/cetak_rapor/');?>";
        f.submit();
        f.refresh();
    }

    function cetakPdf(){
        var f = document.frmRapor;
        f.method = "POST";
        f.action = "<?php echo site_url('Admin/cetak_pdf/');?>";
        f.target = "_blank";
        f.submit();
        f.refresh();
    }
</SCRIPT>
<form name="frmRapor" accept-charset="utf-8">
<div class="container">
    <h1 class="page-header">Admin - Cetak Rapor</h1>
    <div>
        <p>Cari Siswa</p>
        <table class="table table-bordered table-striped" id="tblCust">
            <tr>
                <td>
                    <div id="custom-search-input">
		                <div class="input-group col-md-12">
		                    <input type="text" name='IdSiswa' class="form-control input-lg" placeholder="Nomor Induk Siswa" value="<?php if (!empty($id_siswa)) echo $id_siswa; ?>" />
		                    <span class="input-group-btn">
		                        <button class="btn btn-info btn-lg" type="submit" onClick='searchId();'>
		                            <i class="glyphicon glyphicon-search"></i>
		                        </button>
		                    </span>
		                </div>
		            </div>
                </td>
            </tr>
	</table>
	</div>
    <div>
        <p>Daftar Nilai</p>
        <table class="table table-bordered table-striped" id="tblItem">
            <thead>
				<tr>
	                <th width="31%">Nama Mata Pelajaran</th>
	                <th width="20%">Nilai</th>
	            </tr>
            </thead>
            <tbody>
                <?php if (!empty($nilai)){
                    foreach ($nilai as $key => $value) { ?>
                    <tr>
                        <td>
                            <p><?php echo $value->NAMA_MAPEL; ?></p>
                        </td>
                        <td>
                            <input type="text" name="nilai" readOnly value="<?php echo $value->NILAI?>"></input>
                        </td>
                    </tr>
                    <?php }
                }
                ?>
            </tbody>
	</table>
    <div>
        <p><input type="button" onClick="cetakPdf();" class="btn btn-success" value="Cetak Rapor"></p>
    </div>
    </div>
</div>
</form>