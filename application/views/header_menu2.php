<body>
	<!-- Navbar -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url('Admin');?>"><span class="glyphicon glyphicon-home"></span> Sistem Rapor - Admin</a>
			</div>

			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="dropdown<?php if($menu=='setup'){?> active<?php }?>">
              			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<i class="glyphicon glyphicon-list"></i>
              			Setup Data <span class="caret"></span></a>
          				<ul class="dropdown-menu" role="menu">
            				<li><a href="<?php echo site_url('Admin/siswa');?>">Setup Siswa </a></li>
            				<li><a href="<?php echo site_url('Admin/mata_pelajaran');?>">Setup Mata Pelajaran </a></li>
                    <li><a href="<?php echo site_url('Admin/tahun_ajaran');?>">Setup Tahun Ajaran </a></li>
                    <li><a href="<?php echo site_url('Admin/kelas');?>">Setup Kelas </a></li>
                    <li><a href="<?php echo site_url('Admin/guru');?>">Setup Guru </a></li>
                    <li><a href="<?php echo site_url('Admin/setting_nilai');?>">Setup Batas Nilai </a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo site_url('Admin/mapel_kelas');?>">Setup Mata Pelajaran Per Kelas </a></li>
                    <li><a href="<?php echo site_url('Admin/siswa_kelas');?>">Setup Siswa Per Kelas </a></li>
          				</ul>
            		</li>
					<li <?php if($menu=='entri_nilai'){?>class="active"<?php }?>>
						<a href="<?php echo site_url('Admin/entri_nilai');?>">
						<i class="glyphicon glyphicon-list"></i>
						Entri Nilai Pelajaran</a>
					</li>
          <li <?php if($menu=='cetak_rapor'){?>class="active"<?php }?>>
            <a href="<?php echo site_url('Admin/cetak_rapor');?>">
            <i class="glyphicon glyphicon-list"></i>
            Cetak Rapor</a>
          </li>
					<!-- <li class="dropdown<?php if($menu=='group_arsip'){?> active<?php }?>">
              			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<i class="glyphicon glyphicon-list"></i>
              			Arsip <span class="caret"></span></a>
      				<ul class="dropdown-menu" role="menu">
        				<li><a href="<?php echo site_url('Admin/group_arsip');?>">Grup Arsip </a></li>
        				<li><a href="<?php echo site_url('Admin/group_pilkada');?>">Grup Pilkada </a></li>
        				<li><a href="<?php echo site_url('Admin/group_regulasi');?>">Grup Regulasi </a></li>
                <li><a href="<?php echo site_url('Admin/group_unduh_berkas');?>">Grup Unduh Berkas </a></li>
                <li class="divider"></li>
        				<li><a href="<?php echo site_url('Admin/arsip_pemilu');?>">Arsip Pemilu </a></li>
        				<li><a href="<?php echo site_url('Admin/arsip_pilkada');?>">Arsip Pilkada </a></li>
        				<li><a href="<?php echo site_url('Admin/arsip_regulasi');?>">Arsip Regulasi </a></li>
                <li><a href="<?php echo site_url('Admin/arsip_unduh');?>">Arsip Unduh </a></li>
                <li><a href="<?php echo site_url('Admin/foto_kegiatan');?>">Foto Kegiatan </a></li>
                <li><a href="<?php echo site_url('Admin/foto_sliders');?>">Foto Sliders </a></li>
      				</ul>
          </li>
          <li <?php if($menu=='pengumuman'){?>class="active"<?php }?>>
            <a href="<?php echo site_url('Admin/pengumuman_content');?>">
            <i class="glyphicon glyphicon-list"></i>
            Konten Pengumuman</a>
          </li>
          <li <?php if($menu=='agenda'){?>class="active"<?php }?>>
            <a href="<?php echo site_url('Admin/agenda_content');?>">
            <i class="glyphicon glyphicon-list"></i>
            Konten Agenda</a>
          </li>
          <li <?php if($menu=='tanggal_pemilu'){?>class="active"<?php }?>>
            <a href="<?php echo site_url('Admin/tanggal_pemilu');?>">
            <i class="glyphicon glyphicon-list"></i>
            Tanggal Pemilu</a>
          </li> -->
					<!-- <li class="dropdown<?php if($menu=='catalog'){?> active<?php }?>">
              			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<i class="glyphicon glyphicon-list"></i>
              			Catalog <span class="caret"></span></a>
              				<ul class="dropdown-menu" role="menu">
                				<li class="dropdown-header">Catalog Pre Order</li>
                				<li><a href="<?php echo site_url('catalog_publish_po');?>">Data Produk <span class="label label-success">Publish</span></a></li>
                				<li><a href="<?php echo site_url('catalog_unpublish_po');?>">Data Produk <span class="label label-danger">Unpublish</span></a></li>
                				<li class="divider"></li>
                				<li class="dropdown-header">Catalog Ready Stock</li>
                				<li><a href="<?php echo site_url('catalog_publish_rs');?>">Data Produk <span class="label label-success">Publish</span></a></li>
                				<li><a href="<?php echo site_url('catalog_unpublish_rs');?>">Data Produk <span class="label label-danger">Unpublish</span></a></li>
              				</ul>
            		</li> -->
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown <?php if($menu=='profile'){?> active<?php }?>">
              			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              				<span class="glyphicon glyphicon-user"></span>&nbsp;Account <span class="caret"></span></a>
              			<ul class="dropdown-menu" role="menu">
                			<li><a href="<?php echo site_url('profile');?>"><span class="glyphicon glyphicon-user"></span>&nbsp;Profile</a></li>
                			<li><a href="#"><span class="glyphicon glyphicon-envelope"></span>&nbsp;Pesan</a></li>
                			<li class="divider"></li>
                			<li><a href="<?php echo site_url('Admin/logout'); ?>"><span class="glyphicon glyphicon-off"></span>&nbsp;Keluar</a></li>
              			</ul>
            		</li>
				</ul>
			</div>
		</div>
	</nav>
